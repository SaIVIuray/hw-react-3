import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './components/Home/Home';
import Favorites from './components/Pages/Favorite';
import Card from './components/Pages/Card';
import { useState, useEffect } from 'react';

function App() {
  const [cartItems, setCartItems] = useState(JSON.parse(localStorage.getItem("cartItems")) || []);
  const [favoriteItems, setFavoriteItems] = useState(JSON.parse(localStorage.getItem("favoriteItems")) || []);

  useEffect(() => {
    const updateLocalStorage = () => {
      localStorage.setItem("cartItems", JSON.stringify(cartItems));
      localStorage.setItem("favoriteItems", JSON.stringify(favoriteItems));
    };
    updateLocalStorage();
  }, [cartItems, favoriteItems]);


  return (
    <Router>  
      <Routes>
        <Route
          path="/"
          element={<Home
            cartItems={cartItems}
            setCartItems={setCartItems}
            favoriteItems={favoriteItems}
            setFavoriteItems={setFavoriteItems}
          />}
        />
        <Route
          path="/favorites"
          element={<Favorites
            favoriteItems={favoriteItems}
            cartItemTotal={cartItems.length}
            favoriteItemTotal={favoriteItems.length}
            // addToFavorites={addToFavorites}
            // addToCart={ addToCart}
            // showCartModal={showCartModal}
            setFavoriteItems={setFavoriteItems} 
          />}
        />
        <Route
          path="/card"
          element={<Card 
            cartItems={cartItems}
            cartItemTotal={cartItems.length}
            favoriteItemTotal={favoriteItems.length}
            setCartItems={setCartItems}
          />}
        />
      </Routes>
    </Router>
  );
}

export default App;
