import './Modal.scss'

export default function ModalWrapper({ children, onClose }) {

    function clickFromOutside(ev){
        if (ev.target === ev.currentTarget) {
            onClose()
        }
    }
    
    return(
        <div className="modal-wrapper" onClick={clickFromOutside}>
            {children}
        </div>
        )
}