import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import '../Modal/Modal.scss';
import Header from "./Header";
import ProductList from "../ProductList/ProductList";
import ModalText from "../Modal/ModalText";


export default function Home(props) {
  const { cartItems, favoriteItems, setFavoriteItems, setCartItems } = props;
  const [products, setProducts] = useState([]);
  const [isTextModalOpen, setIsTextModalOpen] = useState(false);

  const sendRequest = async (url) => {
    const response = await fetch(url);
    const products = await response.json();
    return products;
  };
  
  useEffect(() => {
    sendRequest("products.json").then((products) => {
      setProducts(products);
    });
  }, [favoriteItems, cartItems, setFavoriteItems]); 

    
      
    const handleAddToCart = (product) => {
        const indexItem = cartItems.findIndex((item) => item.id === product.id);
      
        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                name: product.name,
                price: product.price,
                image: product.image,
                color:product.color,
                acticle:product.acticle,
                quantity: 1,
                };
                const mergedCartItems = [...cartItems, newItem];
                setCartItems(mergedCartItems);
                localStorage.setItem("cartItems", JSON.stringify(mergedCartItems));
                
            } else {
                const updatedItem = {
                    ...cartItems[indexItem],
                    quantity: cartItems[indexItem].quantity + 1,
                };
                const updatedCartItems = [...cartItems];
                updatedCartItems.splice(indexItem, 1, updatedItem);
      
                setCartItems(updatedCartItems);
                localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
             
            }
        };
      
      
        const handleAddToFavorites = (product) => {
          setFavoriteItems((prevFavorites) => {
            const indexItem = prevFavorites.findIndex((item) => item.id === product.id);
        
            if (indexItem === -1) {
              const newItem = {
                id: product.id,
                name: product.name,
                price: product.price,
                image: product.image,
              };
              const mergedFavoriteItems = [...prevFavorites, newItem];
        
              localStorage.setItem("favoriteItems", JSON.stringify(mergedFavoriteItems));
              return mergedFavoriteItems;
            } else {
              const updatedFavoriteItems = prevFavorites.filter((item) => item.id !== product.id);
        
              localStorage.setItem("favoriteItems", JSON.stringify(updatedFavoriteItems));
        
              return updatedFavoriteItems;
            }
          });
        };
      
        useEffect(() => {
          sendRequest("products.json").then((products) => {
            setProducts(products);
          });
        }, []);

    
        const cartItemTotal = cartItems.reduce((total, item) => total + item.quantity, 0);
        const favoriteItemTotal = favoriteItems.length;
             
        const openTextModal = () => setIsTextModalOpen(true);
        const closeTextModal = () => setIsTextModalOpen(false);

    
          return (
            <div>
              <Header 
                cartItemTotal={cartItemTotal}
                favoriteItemTotal={favoriteItemTotal}
              />
              <ProductList
                products={products}
                cartItems={cartItems}
                favoriteItems={favoriteItems}
                showCartModal={openTextModal}
                addToCart={handleAddToCart}
                addToFavorites={handleAddToFavorites}
                setFavoriteItems={setFavoriteItems}
              />
              {isTextModalOpen && (
                cartItems.map((product) => (
                  <ModalText
                    key={product.id}
                    title={product.name}
                    text={product.color}
                    firstText={'Added to cart'}
                    onClose={closeTextModal}
                  />
                  
                ))
              )}
            </div>
            
          );
        }

        Home.propTypes = {
          products: PropTypes.array,
          cartItems: PropTypes.array,
          favoriteItems: PropTypes.array,
          addToCart: PropTypes.func,
          addToFavorites: PropTypes.func,
          showCartModal: PropTypes.func,
          cartItemTotal: PropTypes.number,
          favoriteItemTotal: PropTypes.number,
          key: PropTypes.number,
          title: PropTypes.string,
          text: PropTypes.string,
          firstText: PropTypes.string,
          onClose: PropTypes.func,
        };