import { faStar, faCartShopping } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { NavLink } from "react-router-dom";
import './Header.scss';

export default function Header({ cartItemTotal, favoriteItemTotal }) {

  return (
    <header className="header">
      <div className="container header-wrapper">
        <h1 className="title">
          <img className='title-img' src="/public/images/icone-vecteur-logo-soins-pour-animaux-compagnie-illustration-fond-noir_683738-5069.jpg" alt="" />
          <NavLink to="/"> Jack&Pets</NavLink></h1>
        
        <div className="icons-wrapper">
          <NavLink to="/"> Home</NavLink>
          <NavLink to="/favorites" className="star-wrapper">
            <FontAwesomeIcon icon={faStar} size="lg" />
            <span>{favoriteItemTotal}</span>
          </NavLink>
          <NavLink to="/card" className="cart-shopping-wrapper">
            <FontAwesomeIcon icon={faCartShopping} size="lg" />
            <span>{cartItemTotal}</span>
          </NavLink>
        </div>
      </div>
    </header>
  );
}
