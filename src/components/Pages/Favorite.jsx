import { useState } from "react";
import Header from "../Home/Header";
import ProductCard from "../ProductСard/ProductCard";
import ModalImage from "../Modal/ModalImage";
import PropTypes from "prop-types";


import "./Favorite.scss";

export default function Favorites({favoriteItems, cartItemTotal, favoriteItemTotal, addToCart, addToFavorites, showCartModal, setFavoriteItems}) {

  const [deleteItem, setDeleteItem] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  const handleDeleteItem = (item) => {
    setDeleteItem(item);
    setIsDeleteModalOpen(true);
  };

  const handleConfirmDelete = () => {
    const updatedFavorites = favoriteItems.filter((item) => item.id !== deleteItem.id );
    setFavoriteItems(updatedFavorites); 
  
    setIsDeleteModalOpen(false);
    setDeleteItem(null);
  };

  const handleCloseModal = () => {
    setIsDeleteModalOpen(false);
    setDeleteItem(null);
  };

  return (
    <div>
      <Header cartItemTotal={cartItemTotal} favoriteItemTotal={favoriteItemTotal} />
      <ul className="Favorite">
        {Array.isArray(favoriteItems) ? (
          favoriteItems.map((item) => (
            <li className="Favorite_Items" key={item.id}>
              <ProductCard
                product={item}
                inCart={false}
                inFavorites={false}
                addToCart={addToCart}
                addToFavorites={addToFavorites}
                showCartModal={showCartModal}
                onDelete={() => handleDeleteItem(item)}
                showDeleteIconStar={true} 
              />
            </li>
          ))
        ) : (
          <p>There are no featured products yet.</p>
        )}
      </ul>
      {isDeleteModalOpen && (
  <ModalImage
    className="delete-modal"
    title="Removing a product"
    text={`Are you sure you want to delete ${
      deleteItem ? deleteItem.name : "этот товар"
    }?`}
    onClose={handleCloseModal}
    onConfirm={handleConfirmDelete} 
  />
)}
    </div>
  );
}

Favorites.propTypes = {
  cartItems: PropTypes.array,
  cartItemTotal: PropTypes.number,
  favoriteItemTotal: PropTypes.number,
  addToCart: PropTypes.func,
  addToFavorites: PropTypes.func,
  showCartModal: PropTypes.func,
  setFavoriteItems: PropTypes.func,
};
