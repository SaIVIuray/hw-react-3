import { useState } from 'react';
import Header from '../Home/Header';
import ProductCard from '../ProductСard/ProductCard';  
import PropTypes from "prop-types";
import ModalImage from '../Modal/ModalImage';
import './Card.scss';

export default function Cart ({ cartItems, cartItemTotal, favoriteItemTotal, addToCart, addToFavorites, showCartModal, setCartItems }) {

  const [deleteItem, setDeleteItem] = useState(null);
  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);

  const handleDeleteItem = (item) => {
    setDeleteItem(item);
    setIsDeleteModalOpen(true);
  };

  const handleConfirmDelete = () => {
    const updatedCartItems = cartItems.filter((item) => item.id !== deleteItem.id);
    setCartItems(updatedCartItems);
    
    setIsDeleteModalOpen(false);
    setDeleteItem(null);
  };

  const handleCloseModal = () => {
    setIsDeleteModalOpen(false);
    setDeleteItem(null);
  };

  return (
    <div>
      <Header cartItemTotal={cartItemTotal} favoriteItemTotal={favoriteItemTotal} />
      <ul className='Card'>
        {Array.isArray(cartItems)  ? (
          cartItems.map((item) => (
            <li className='Card_Items' key={item.id}>
              <ProductCard
               product={item}
               inCart={true}
               inFavorites={false}
               addToCart={addToCart}
               addToFavorites={addToFavorites}
               showCartModal={showCartModal}
               onDelete={() => handleDeleteItem(item)} 
               showDeleteIconXmark={true} 
               setCartItems={setCartItems}
              />
            </li>
          ))
        ) : (
          <h1>В корзине нет товаров.</h1>
        )}
      </ul>
      {isDeleteModalOpen && (
  <ModalImage
    className="delete-modal"
    title="Removing a product"
    text={`Are you sure you want to delete ${
      deleteItem ? deleteItem.name : "этот товар"
    }?`}
    onClose={handleCloseModal}
    onConfirm={() => handleConfirmDelete()}  
  />
)}
    </div>
  );
}

Cart.propTypes = {
  cartItems: PropTypes.array,
  cartItemTotal: PropTypes.number,
  favoriteItemTotal: PropTypes.number,
  addToCart: PropTypes.func,
  addToFavorites: PropTypes.func,
  showCartModal: PropTypes.func,
  setCartItems: PropTypes.func,
};
